########################################################################
# Taller Flash N°4
# N reinas
# Autor: Gabriel Lichtenstein
# Fecha: 01/06/2016
########################################################################
# usage: python3 NReinas.py									   		   #
########################################################################
#
#
#
########################################################################
def NReinas(n):
	#arma un tablero incial (Ti), sin reinas:
	Ti = tablero_inicial(n)
	
	#dado un tablero inicial armar un tablero final con n reinas (Tf):
	Tf = tablero_final(n,Ti)
	
	return (Tf)
########################################################################
#
#
#
########################################################################
def tablero_inicial(n): 
	'''
	Crea una matriz "tablero" de n listas y n elementos en cada lista. 
	
	el tablero tiene:
	filas: cada lista del tablero representa una fila, hay n filas.
	columnas: los indices de cada fila, representan una columna y
	hay n columnas en cada fila.
	
	El tablero es creado sin reinas -> todos las posiciones son False.
	'''
	
	tablero = []
	for filas in range (0,n):
		col = []
		for columnas in range (0,n):
			col.append(False) #en cada columna de la fila colocar False.
		tablero.append(col)	#agregar n filas al tablero
	return tablero
########################################################################
#
#
#
########################################################################
def tablero_final(n, T):
	'''
	Esta función devuelve UNA solución al problema NReinas mediante 
	la implementación de la técnica de Backtracking:
	
	Dado un tablero_inicial (T) y (n) reinas para colocar:
	1) colocó una reina (n) -> inserto un True en la primera posicion 
	del tablero: T[0][0] -> fila=0, col=0
	
	2) En este estado de ejecución me quedan n-1 reinas para colocar en
	el tablero_final.
	
	De forma recursiva se vuelve a llamar a tablero_final pero 
	esta vez con n-1 reinas. 
	Se coloca True en la fila=1, col=0 -> T1[1][0]
	
	3) Se comprueba si la Reina colocada en el paso 1) quedo bajo ataque 
	de la Reina colocada en el paso 2), sino lo esta, 
	la Reina queda colocada, tablero_final(n-1,T) devuelve True y pasa
	a colocar una Reina en la fila siguiente.
	Pero si esta bajo ataque, se mueve la Reina a la próxima columna y 
	se repite el paso 3) hasta encontrar una posicion libre o hasta la 
	última colmna de la fila (len(T)).
	
	4) Si no se encuentra una columna para colocar la Reina en la fila
	evaluada, se realiza Backtracking -> 
	tablero_final(n-1,T) devuelve False y remueve la Reina colocada en 
	la fila anterior y la coloca en la siguiente columna.
	
	5) Cuando ya no hay más reinas para colocar, la función termina y 
	devuelve un tablero final con n reinas.	
	'''
	if (n == 0): # si no tengo más reinas para poner en el tablero,
		return T # devuelvo el tablero_final con NReinas
	
	else:
		if n > 0: # checkpoint para que fila no se salga de rango.
			fila = abs(len(T) - n)
			for col in range(0, len(T)):
				
				# REVISAR que la columna, la fila y las diagonales 
				# esten libres de otras reinas:
				if (libre_col(col,T) 
				and libre_fila(fila,T)
				and libre_diagonal(fila,col,T)):
				
					# Si la revisión da OK, la posición es candidata
					# para colocar una Reina y se coloca un True.
					T[fila][col] = True
					
					# Backtracking:
					if tablero_final(n-1, T):
						return T
					else:
						T[fila][col] = False
########################################################################
#
#
#
########################################################################
def libre_col(col,T):
	'''chequea que no haya otra reina en la misma columna, 
	   devuelve True si la columna esta libre'''
	i = 0
	while (i < len(T)) and (T[i][col] == False):
		i+=1
		
	return (i == len(T))
########################################################################
#
#
#
########################################################################	
def libre_fila(fila,T):
	'''chequea que no haya otra reina en la misma fila, 
	   devuelve True si la fila esta libre'''
	i = 0
	while (i < len(T)) and (T[fila][i] == False):
	   i+=1
		
	return (i == len(T))
########################################################################
#
#
#
########################################################################	
def libre_diagonal(i, j, T):
	libre = True #estado de las posiciones diagonales a la reina
	'''
	Resulta que los indices [fila][col] de una posicion en la matriz, 
	estan a la misma distancia de una posicion diagonal [i][j].
	Por ejemplo T[0][0] y T[1][1] son posiciones diagonales ->
	abs([0]-[1]) == abs([0]-[1]) 
	
	Si en las posiciones diagonles hay un True (reina) esa diagonal no
	esta libre -> libre = False	
	'''
	for fila in range(0,len(T)): #recorre las filas
		for col in range(0,len(T)):
			if ((abs(fila - i) == abs(col - j)) and T[fila][col]==True):
				libre = False
	return (libre)
########################################################################
#
#
#
########################################################################
#EJECUTAR NREINAS
########################################################################
# 1) Preguntar por consola el valor de N:
flag = False
while not (flag):
	try: # chequear que se un int >= 4:
		n = int(input("Ingrese el número de reinas: "))
		if n >= 4:
			flag = True
		else:
			print("Error: Número inválido. Use un número entero >= 4")
	except ValueError :
		print("Error: Número inválido. Use un número entero >= 4")

# 2) Imprimir en consola el resultado, fila por fila:
for fila in NReinas(n):
	print(fila)
########################################################################
